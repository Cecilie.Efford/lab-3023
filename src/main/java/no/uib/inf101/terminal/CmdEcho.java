package no.uib.inf101.terminal;

public class CmdEcho implements Command {
    @Override
    public String run(String[] args) {
        String str = "";
        for (String arg : args) {
            str += String.join(" ", arg + " ");


        }
        System.out.print(str);
        return str;

    }

    @Override
    public String getName() {
        return "echo";
    }
}
